<?php get_header() ?>
    <main>
    <h1>Essa é a Search Page </h1>
        <?php 
        get_search_form( );
        if ( have_posts() ) : 
            while ( have_posts() ) : the_post(); ?>
                <h3><?php the_title(); ?> </h3>
                <a href="<?php the_permalink(); ?>"> Link </a>
     <?php endwhile; 
            else : ?>
	            <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?> <wbr> </p>
     <?php endif; 
            echo paginate_links()
        ?>
    </main>
 <?php get_footer() ?>