<?php
/*
Template Name: Posts
*/
?>
<?php get_header() ?>
    <main>
    <?php the_title(); ?>
    <?php the_content(); ?>
    </main>
 <?php get_footer() ?>

 